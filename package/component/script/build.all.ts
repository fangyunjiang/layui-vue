import { UserConfigExport } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import { resolve } from "path";

export default (): UserConfigExport => {
  return {
    publicDir: false,
    plugins: [vue(), vueJsx()],
  };
};
